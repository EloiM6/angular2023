import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Exemple1Component } from './view/exemple1/exemple1.component';
import { ExempleDatabindingComponent } from './view/exemple-databinding/exemple-databinding.component';
import { ExempleDatabindingFillComponent } from './view/exemple-databinding-fill/exemple-databinding-fill.component';
import { ExempleDatabindingPareComponent } from './view/exemple-databinding-pare/exemple-databinding-pare.component';
import {FormsModule} from "@angular/forms";
import { CanvitamanyComponent } from './view/canvitamany/canvitamany.component';
import { ExempleDirectivesComponent } from './view/exemple-directives/exemple-directives.component';
import { Component1Component } from './view/component1/component1.component';
import { Component2Component } from './view/component2/component2.component';
import { Component3Component } from './view/component3/component3.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    Exemple1Component,
    ExempleDatabindingComponent,
    ExempleDatabindingPareComponent,
    ExempleDatabindingFillComponent,
    CanvitamanyComponent,
    ExempleDirectivesComponent,
    Component1Component,
    Component2Component,
    Component3Component,

  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
