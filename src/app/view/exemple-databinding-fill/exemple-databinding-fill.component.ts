import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgModel} from "@angular/forms";
@Component({
  selector: 'app-exemple-databinding-fill',
  templateUrl: './exemple-databinding-fill.component.html',
  styleUrls: ['./exemple-databinding-fill.component.css']
})
export class ExempleDatabindingFillComponent {
  @Input() missatge!: string;
  @Output() missatgeChange = new EventEmitter<string>();

  constructor() { }

  onMissatgeChange(missatge: string) {
    this.missatge = missatge;
    console.log(missatge);
    this.missatgeChange.emit(missatge);
  }
}
