import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExempleDatabindingFillComponent } from './exemple-databinding-fill.component';

describe('ExempleDatabindingFillComponent', () => {
  let component: ExempleDatabindingFillComponent;
  let fixture: ComponentFixture<ExempleDatabindingFillComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExempleDatabindingFillComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExempleDatabindingFillComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
