import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExempleDirectivesComponent } from './exemple-directives.component';

describe('ExempleDirectivesComponent', () => {
  let component: ExempleDirectivesComponent;
  let fixture: ComponentFixture<ExempleDirectivesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExempleDirectivesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExempleDirectivesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
