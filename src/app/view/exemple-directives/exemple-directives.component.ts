import { Component } from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-exemple-directives',
  templateUrl: './exemple-directives.component.html',
  styleUrls: ['./exemple-directives.component.css']
})
export class ExempleDirectivesComponent {
  mostrarParagraf = false;
  llistaItems = ['Marc Albareda', 'Gregorio Santamaria', 'Dani Fernandez', 'Antonio Alcalà'];
  mostrarVerd: boolean = true;
  mostrarBlau: boolean = false;
  colorText: string = 'hotpink';
  colorFons: string = 'yellow';
  mostrarComponent = 'component1';
  nom : string = '';
  email : string = 'jajaj';
  toggleParagraf() {
    this.mostrarParagraf = !this.mostrarParagraf;
  }


  enviarFormulari(f:NgForm) {

    //Amb ngModel associem la variable per mostrar valor
    // Però per recollir els canvis usem una funció.
    this.nom=f.value.nom;
    this.email = f.value.email;
    console.log(f.valid);
  }
}
