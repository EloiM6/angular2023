import { Component } from '@angular/core';

@Component({
  selector: 'app-exemple-databinding-pare',
  templateUrl: './exemple-databinding-pare.component.html',
  styleUrls: ['./exemple-databinding-pare.component.css']
})
export class ExempleDatabindingPareComponent {
  missatge = 'Aquest és el missatge inicial';
  constructor() { }
}
