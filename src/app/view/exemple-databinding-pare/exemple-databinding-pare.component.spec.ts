import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExempleDatabindingPareComponent } from './exemple-databinding-pare.component';

describe('ExempleDatabindingPareComponent', () => {
  let component: ExempleDatabindingPareComponent;
  let fixture: ComponentFixture<ExempleDatabindingPareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExempleDatabindingPareComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExempleDatabindingPareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
