import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-canvitamany',
  templateUrl: './canvitamany.component.html',
  styleUrls: ['./canvitamany.component.css']
})
export class CanvitamanyComponent {
// El simbol ! elimina un valor null o undefined de la declaració
  @Input()  size!: number | string;
  // EventEmitter serveix per registrar esdeveniments de components.
  // Sempre va associat amb la directiva output
  @Output() sizeChange = new EventEmitter<number>();
  canvitamany(nouvalor: number) {
    this.size = Math.min(40, Math.max(8, +this.size + nouvalor));
    this.sizeChange.emit(this.size);
  }
  dec() { this.canvitamany(-1); }
  inc() { this.canvitamany(+1); }

  constructor() { }

  ngOnInit(): void {
  }
}
