import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CanvitamanyComponent } from './canvitamany.component';

describe('CanvitamanyComponent', () => {
  let component: CanvitamanyComponent;
  let fixture: ComponentFixture<CanvitamanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CanvitamanyComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CanvitamanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
