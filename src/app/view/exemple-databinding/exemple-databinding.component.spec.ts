import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExempleDatabindingComponent } from './exemple-databinding.component';

describe('ExempleDatabindingComponent', () => {
  let component: ExempleDatabindingComponent;
  let fixture: ComponentFixture<ExempleDatabindingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExempleDatabindingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExempleDatabindingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
