import { Component } from '@angular/core';

@Component({
  selector: 'app-exemple-databinding',
  templateUrl: './exemple-databinding.component.html',
  styleUrls: ['./exemple-databinding.component.css']
})
export class ExempleDatabindingComponent {
  text : string = "Eres un pollerudo";
  imatgeUrl : string = 'https://www.shutterstock.com/image-vector/eggs-isolated-on-white-background-600w-1889247892.jpg';
  missatge : string = ""
  mostraMissatge() {
    this.missatge = 'Hola, aquest és un missatge!';
  }
}
