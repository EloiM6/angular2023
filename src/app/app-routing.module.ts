import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {ExempleDirectivesComponent} from "./view/exemple-directives/exemple-directives.component";
import {ExempleDatabindingPareComponent} from "./view/exemple-databinding-pare/exemple-databinding-pare.component";
import {ExempleDatabindingComponent} from "./view/exemple-databinding/exemple-databinding.component";

const routes : Routes = [
  {path : 'DataBinding', component : ExempleDatabindingComponent},
  {path : 'DataBindingBidreccional', component : ExempleDatabindingPareComponent},
  //{path : 'CanviTamany', component : CanvitamanyComponent},
  {path : 'Directives', component : ExempleDirectivesComponent},
  //{path : '**', component : PaginaInexistentComponent},

];

@NgModule({
  declarations: [],

    exports: [RouterModule],

  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
